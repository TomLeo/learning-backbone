
Marionette
==========

Marionette.View
---------------

Marionette.triggerMethod function, which triggers the event and a
corresponding "on{EventName}" method

ui hash
```````

specify a ui hash in your view that maps UI elements by their jQuery
selectors

.. code-block:: javascript

    Marionette.View.extend({
        ui: {
            checkbox: "input[type=checkbox]"
        }
    });

Reference inside your view via ``this.getUI('elementName'):``

You can also use the ui hash values from within events and trigger
keys using the "@ui.elementName": syntax

listen to model/collection events
`````````````````````````````````

.. code-block:: javascript

    modelEvents: {
        "change": "modelChanged"
    },

    collectionEvents: {
        "add": "modelAdded"
    }

Marionette.LayoutView
---------------------

.. code-block:: javascript

    Marionette.LayoutView.extend({

        regions: {
            menu: "#menu",
            content: "#content"
        }

    })

After ``layoutView.render()``:
``layoutView.getRegion('menu').show(new MenuView(), options);``
or
``layoutView.showChildView('menu', new MenuView(), options);``
